#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

	ofSetVerticalSync(true);
	ofBackground(54, 54, 54);
	ofSetFrameRate(60);

    //soundStream.listDevices();
    //soundStream.setDeviceID(0); //bear in mind the device id corresponds to all audio devices, including  input-only and output-only devices.
    int ticksPerBuffer = BUFFER_SIZE/64;
    bufferSize = ofxPd::blockSize()*ticksPerBuffer; //pdBlock-64
    inChan  = 2;
	outChan = 2;
	sampleRate = SR;

    int midiMin = 21;
    int midiMax = 108;
    filterBank.setup(bufferSize, midiMin, midiMax, inChan, BANDWITH, sampleRate, 1.0);

	soundStream.setup(this, outChan, inChan, sampleRate, bufferSize, ticksPerBuffer);	// 0 output channels - 2 input channels - 44100 samples per second - 256 samples per buffer - 4 num buffers (latency)
    core.setup(outChan, inChan, sampleRate, ticksPerBuffer);


    setGUI();
    gui1->setDrawBack(false);

    filename = "-none-";
    xmlName = "-none-";

   	bufferCounter	= 0;
	drawCounter		= 0;

    textInput = NULL;
}

//--------------------------------------------------------------

void testApp::update(){


}

//--------------------------------------------------------------
void testApp::draw(){

	ofSetColor(225);
	ofNoFill();
	ofDrawBitmapString("FILE LOADED: "+ filename , 15, 60);
	ofDrawBitmapString("Set to: "+ ofToString(filterBank.pitchDev) , 140, 400);
	ofDrawBitmapString("XML file: "+ xmlName , 110, 470);

	// draw the left channel:
	{
        ofPushStyle();
            ofPushMatrix();
            ofTranslate(300, 15, 0);
            ofSetColor(225);
            ofDrawBitmapString("Left Channel", 4, 18);
            ofSetLineWidth(1);
            ofRect(0, 0, 256, 200);
            ofSetColor(245, 58, 135);
            ofSetLineWidth(3);
                ofBeginShape();
                for (int i = 0; i < filterBank.left.size(); i++){
                    ofVertex(i/(bufferSize/256), 100 - filterBank.left[i]*180.0f);
                }
                ofEndShape(false);
            ofPopMatrix();
        ofPopStyle();
	}
	// draw the right channel:
	{
        ofPushStyle();
		ofPushMatrix();
		ofTranslate(600, 15, 0);
		ofSetColor(225);
		ofDrawBitmapString("Right Channel", 4, 18);
		ofSetLineWidth(1);
		ofRect(0, 0, 256, 200);
		ofSetColor(245, 58, 135);
		ofSetLineWidth(3);
			ofBeginShape();
			for (int i = 0; i < filterBank.right.size(); i++){
				ofVertex(i/(bufferSize/256), 100 - filterBank.right[i]*180.0f);
			}
			ofEndShape(false);
		ofPopMatrix();
        ofPopStyle();
	}
	///Draw FilterBank
	{
        ofPushStyle();
        ofPushMatrix();
        ofTranslate (300,250,0);
        //*
        filterBank.draw(650,400);
        //*
        ofPopMatrix();
        ofPopStyle();
	}
	drawCounter++;
	ofSetColor(225);

	string reportString =  "Sampling Rate: "+ ofToString(SR) +"\nBuffer size: "+ ofToString(bufferSize) +"\nbuffers received: "+ofToString(bufferCounter)+"\ndraw routines called: "+ofToString(drawCounter)+"\nticks: " + ofToString(soundStream.getTickCount());
	ofDrawBitmapString(reportString, 32, 675);


}

//--------------------------------------------------------------
void testApp::audioIn(float * input, int bufferSize, int nChannels){

}


void testApp::audioOut(float * output, int bufferSize, int nChannels){
    	core.audioRequested(output, bufferSize, nChannels);
    	filterBank.measure(output);
        bufferCounter++;

//		for (int i = 0; i < bufferSize; i++){
//			left[i] = output[i*nChannels    ] ;
//            right[i] = output[i*nChannels + 1];
//		}

}

//--------------------------------------------------------------
void testApp::keyPressed  (int key){

     if(gui1->hasKeyboardFocus()) {
        return;
     }
    switch (key){
        case 'q':
            filterBank.showAll = !filterBank.showAll;
			break;
        default:
            break;
    }
}


//--------------------------------------------------------------
void testApp::exit(){
    printf("------- CLOSING...\n");
    delete gui1;
    core.exit();
    soundStream.stop();
    soundStream.close();
    filterBank.exit();
    cout<<"CLOSED SUCCESSFULLY!"<<endl;
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}
//--------------------------------------------------------------
void testApp::setGUI(){
    float dim = 16;
    float xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
    float length = 255-xInit;

    gui1 = new ofxUICanvas(10, 10, length+xInit, ofGetHeight());

	gui1->addWidgetDown(new ofxUILabel("Filter Bank", OFX_UI_FONT_LARGE));
    gui1->addSpacer(length-xInit, 2);

    gui1->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));//space
	gui1->addWidgetDown(new ofxUILabel("OPEN FILE", OFX_UI_FONT_MEDIUM));
	//gui1->setWidgetFontSize(OFX_UI_FONT_LARGE);
    //-------
	gui1->setWidgetFontSize(OFX_UI_FONT_MEDIUM);
	gui1->addTextInput("OPEN FILE", "*.wav", length-xInit);

	gui1->addButton("PLAY", false, dim, dim);
	gui1->addButton("STOP", false, dim, dim);
	gui1->addSpacer(length-xInit, 2);
	gui1->addRangeSlider("NOTE-RANGE", 21, 108, 21, 108, length-xInit,dim);
	gui1->addSlider("VOLUME", 0.0, 1.5, 1.0, length-xInit, dim);
	gui1->addSlider("SMOOTHING", 0.0, 1.0, 0.7, length-xInit,dim);
	gui1->addSlider("MASK", 0.0, 1.0, 0.0, length-xInit,dim);
	//gui1->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));//space


	gui1->addSlider("MAX-AMP", 0.0, 0.5, 0.2, length-xInit,dim);
	gui1->addSlider("TRESHOLD", 0.0, 0.5, 0.1, length-xInit,dim);
	gui1->addSlider("PITCH-DEV", -1.0, 1.0, 0.0, length-xInit,dim);

	gui1->addButton("SET-DEV", false, dim, dim);
	gui1->addSpacer(length-xInit, 2);

	gui1->addWidgetDown(new ofxUILabel("SETTINGS", OFX_UI_FONT_MEDIUM));
	gui1->addTextInput("SETTINGS", "*.xml", length-xInit);
	gui1->addButton("SAVE", false, dim, dim);
	gui1->addButton("LOAD", false, dim, dim);
	gui1->addButton("RESET", false, dim, dim);
	gui1->addSpacer(length-xInit, 2);

    gui1->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));//space
	gui1->addToggle( "AUDIO-ON/OFF", true, dim, dim);

	ofAddListener(gui1->newGUIEvent,this,&testApp::guiEvent);

}

//--------------------------------------------------------------
void testApp::guiEvent(ofxUIEventArgs &e){
    string name = e.widget->getName();
	int kind = e.widget->getKind();
	static float  pitchFader = 0.0;

	cout << "got event from: " << name << endl;

    if(name == "OPEN FILE"){
        //cout << "openfilexing" << endl;

        ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
        cout<< textinput->getInputTriggerType()<< endl;
        if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_ENTER) {
            cout << "ON ENTER: "<< endl;
            filename = textinput->getTextString();
            //            ofUnregisterKeyEvents((testApp*)this);
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_FOCUS){
            cout << "ON FOCUS: ";
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_UNFOCUS) {
            cout << "ON BLUR: ";
//            ofRegisterKeyEvents(this);
        }

    }else if (name == "PLAY"){
       	ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (value) core.openPlayFile(filename);
    }else if (name == "STOP"){
       	ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (!value) core.pd.sendBang("stopOF");
    }else if (name == "NOTE-RANGE"){
        ofxUIRangeSlider *rSlider = (ofxUIRangeSlider *) e.widget;
        filterBank.midiMinVar = (int) rSlider->getScaledValueLow();
        rSlider->setValueLow(filterBank.midiMinVar);
        filterBank.midiMaxVar = (int) rSlider->getScaledValueHigh();
        rSlider->setValueHigh(filterBank.midiMaxVar);
    }else if (name == "VOLUME"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        float value = slider->getScaledValue();
        core.pd.sendFloat("volOF", value);
    }else if (name == "SMOOTHING"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        filterBank.smoothAmnt = slider->getScaledValue();
    }else if (name == "MASK"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        filterBank.maskAmnt = slider->getScaledValue();
    }else if (name == "PITCH-DEV"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        pitchFader = slider->getScaledValue() ;
    }else if (name == "SET-DEV"){
       	ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (value){
		    filterBank.pitchDev = pitchFader;
            filterBank.setPitchDev();
        }
    }else if (name == "MAX-AMP"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        filterBank.estimateMax = slider->getScaledValue();
    }else if (name == "TRESHOLD"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        filterBank.treshold = slider->getScaledValue() * filterBank.estimateMax;
       // filterBank.treshold = slider->getScaledValue();
    }else if (name == "AUDIO-ON/OFF"){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
		bool value = toggle->getValue();
		if (value) soundStream.start();
		else soundStream.stop();
    }else if (name == "SETTINGS"){
        ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
        if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_ENTER)
        {
            cout << "ON ENTER: "<< endl;
            xmlName = textinput->getTextString();
            //            ofUnregisterKeyEvents((testApp*)this);
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_FOCUS){
            cout << "ON FOCUS: ";
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_UNFOCUS){
            cout << "ON BLUR: ";
//            ofRegisterKeyEvents(this);
        }
    }else if (name == "RESET"){
         gui1->loadSettings("./settings/default.xml");
         printf("Settings RESETED\n");
    }else if (name == "SAVE"){
        string name = "./settings/"+xmlName;
        gui1->saveSettings(name);
        printf("Settings SAVED\n");
    }else if (name == "LOAD"){
        string name = "./settings/"+xmlName;
        gui1->loadSettings(name);
        printf("Settings LOADED\n");
    }

}
//--------------------------------------------------------------

void testApp::mouseMoved(int x, int y ){

//    float value = ofMap (x, 0, ofGetWidth(), 21, 120.0);
//    core.pd.sendFloat("oscOF", int(value));
//    printf("%i\n", int(value));

}


//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
   // enerMax = 0;
    //core.pd.sendBang("playOF");
   // printf("mousePresed\n");

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}

