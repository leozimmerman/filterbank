#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"
#include "AppCore.h"
#include "ofxUI.h"
#include "Filterbank.h"

extern "C" {
    #include "fb_funcs.h"
}

#define BANDWITH  1.0
#define BUFFER_SIZE 1024
#define LIN2dB(x) (double)(20. * log10(x))
#define SR 44100


class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		void exit();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void audioIn(float * input, int bufferSize, int nChannels);
        void audioOut(float * input, int bufferSize, int nChannels);

        void setGUI();
        ofxUICanvas *gui1;

        void guiEvent(ofxUIEventArgs &e);

		ofSoundStream soundStream;
		AppCore core;
		Filterbank filterBank;

		int		sampleRate;
		int bufferSize;
		int inChan;
		int outChan;

        int bufferCounter;
		int drawCounter;

        string filename;
        string xmlName;
    
        ofxUITextInput *textInput;


};

#endif

