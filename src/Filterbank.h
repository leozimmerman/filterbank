#ifndef FILTERBANK_H
#define FILTERBANK_H

#include "ofMain.h"
#define LIN2dB(x) (double)(20. * log10(x))

extern "C" {
    #include "fb_funcs.h"
}

class Filterbank
{
    public:

      //  Filterbank();
        //virtual ~Filterbank();

        void setup(int iBufferSize, int iMidiMin, int iMidiMax, int iChans, float iBw, int iSR, float iGain);
        void draw(int w, int h);
        void exit();

        void setPitchDev();
        void measure(float * iBuffer);
        string midiToNote (int midi);

		int	sampleRate;
		int bufferSize;
		int channels;

		float bw;
        float gain;
        float smoothAmnt, treshold, estimateMax, maskAmnt, pitchDev, pitchFader;
        int midiMin, midiMax, midiMinVar, midiMaxVar;

		bool showAll;

		vector <float> left;
		vector <float> right;


    protected:

    private:
        RESONDATA ***fdata;
        vector<float> energies;
		vector<float> smth_energies;
		vector<float> log_smth_energies;



};

#endif // FILTERBANK_H
